const express = require("express");
const {
  getComments,
  getComment,
  createComment,
  updateComment,
  deleteComment,
} = require("../controllers/comments");
const {
  createReply,
  getReplies
} = require("../controllers/replies");
const Comment = require("../models/Comment");

// Include other resource routers
const replyRouter = require("./replies");

const router = express.Router({
  mergeParams: true,
});
const advancedResults = require("../middleware/advancedResults");
const { protect, authorize } = require("../middleware/auth");

// Re-route into other resource routers
router.use("/:commentId/replies", replyRouter);

router
  .route("/")
  .get(
    // advancedResults(Comment, {
    //   path: "children",
    //   select: "tag text createdAt post user parentId",
    // }),
    getComments
  )
  .post(protect, authorize("user", "moderator", "admin"), createComment);

router
  .route("/:id")
  .get(getComment)
  .put(protect, authorize("user", "moderator", "admin"), updateComment)
  .delete(protect, authorize("user", "moderator", "admin"), deleteComment);

module.exports = router;
