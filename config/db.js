const mongoose = require('mongoose');

// switch to new DB for collectives
const connectDB = async () => {
    
    const conn = await mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    });

    console.log(`MongoDB Connected: ${conn.connection.host}`);
};

module.exports = connectDB;