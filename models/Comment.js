const mongoose = require("mongoose");
const Populate = require("../utils/autopopulate");

const CommentSchema = new mongoose.Schema({
  text: {
    type: String,
    required: [true, "Please enter a comment"]
  },
  tag: {
    type: [String],
    enum: ["Comment", "Reply"]
  },
  slug: String,
  createdAt: {
    type: Date,
    default: Date.now
  },
  replBody : Object,
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true
  },
  post: {
    type: mongoose.Schema.ObjectId,
    ref: 'Post',
    required: true
  },
  parentId: {
    type: mongoose.Schema.ObjectId,
    ref: "Comment",
    required: false
  }
},
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  });

// Reverse populate with virtuals for comments
// CommentSchema.virtual("children", {
//   ref: "Comment",
//   localField: "_id",
//   foreignField: "parentId",
//   justOne: false
// });



module.exports = mongoose.model("Comment", CommentSchema);
