const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Comment = require("../models/Comment");
const User = require("../models/User");
const Post = require("../models/Post");

// @desc    Get all replies for a comment
// @route   GET /api/v1/comments
// @route   GET /api/v1/posts/:postId/comments/:commentId/replies
// @access  Public
exports.getReplies = asyncHandler(async (req, res, next) => {
    if (req.params.parentId) {
        // might want to change for _id instead of the parent id
        const replies = await Comment.find({ parentId: req.params.parentId });
        return res.status(200).json({
            success: true,
            count: replies.length,
            data: replies
        });
    } else {
        console.log(`not working`);
        res.status(200).json(res.advancedResults);
    }
});

// @desc    Create reply on comment
// @route   POST /api/v1/posts/:postId/comments/:commentId/replies
// @access  Private
exports.createReply = asyncHandler(async (req, res, next) => {
    req.body.post = req.params.postId
    req.body.parentId = req.params.commentId;
    req.body.user = req.user.id;
    console.log(req.body);

    const comment = await Comment.findById(req.params.commentId);

    if (!comment) {
        return next(
            new ErrorResponse(`No comment with the id of ${req.params.commentId}`, 404)
        );
    }

    const reply = await Comment.create(req.body);

    res.status(200).json({
        success: true,
        data: reply,
    });
});
