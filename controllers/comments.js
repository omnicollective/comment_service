const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Comment = require("../models/Comment");
const User = require("../models/User");
const Post = require("../models/Post");

// @desc    Get all comments for a post
// @route   GET /api/v1/comments
// @route   GET /api/v1/posts/:postId/comments
// @access  Public
exports.getComments = asyncHandler(async (req, res, next) => {
  if (req.params.postId) {
    const comments = await Comment.find({ post: req.params.postId }).populate({
      path: "children",
      select: "tag text createdAt post user parentId"
    });
    let comments2  = JSON.parse(JSON.stringify({data: comments}))
    for (let i = 0; i < comments2.data.length; i++){
      const user = await User.findById(comments2.data[i].user);
      let username = JSON.parse(JSON.stringify({data: user})).data.username;
      let addUN  = {
        username: username ? username : "ERROR FINDING USER",
      }
      comments2.data[i] = {...comments2.data[i], ...addUN};
    }
    console.log(comments2.data)
    return res.status(200).json({
      success: true,
      count: comments2.length,
      data: comments2,
    });
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc    Get single comment
// @route   GET /api/v1/comments/:id
// @access  Public
exports.getComment = asyncHandler(async (req, res, next) => {
  const comment = await Comment.findById(req.params.id).populate({
    path: "post",
    select: "name text",
  });

  if (!comment) {
    return next(
      new ErrorResponse(`No comment with the id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({
    success: true,
    data: comment,
  });
});

// @desc    Create comment on post
// @route   POST /api/v1/posts/:postId/comments
// @access  Private
exports.createComment = asyncHandler(async (req, res, next) => {
  req.body.post = req.params.postId;
  req.body.user = req.user.id;

  const post = await Post.findById(req.params.postId);

  if (!post) {
    return next(
      new ErrorResponse(`No post with the id of ${req.params.postId}`, 404)
    );
  }

  const comment = await Comment.create(req.body);

  res.status(200).json({
    success: true,
    data: comment,
  });
});

// @desc    Update comment
// @route   PUT /api/v1/comments/:id
// @access  Private
exports.updateComment = asyncHandler(async (req, res, next) => {
  let comment = await Comment.findById(req.params.id);
  console.log(req.body);

  if (!comment) {
    return next(
      new ErrorResponse(`No comment with the id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is comment owner
  if (comment.user.toString() !== req.user.id && req.user.role !== "admin") {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to update comment ${comment._id}`,
        401
      )
    );
  }

  comment = await Comment.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: comment,
  });
});

// @desc    Delete comment
// @route   DELETE /api/v1/comments/:id
// @access  Private
exports.deleteComment = asyncHandler(async (req, res, next) => {
  const comment = await Comment.findById(req.params.id);

  if (!comment) {
    return next(
      new ErrorResponse(`No comment with the id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is review owner
  if (comment.user.toString() !== req.user.id && req.user.role !== "admin") {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to delete comment ${comment._id}`,
        401
      )
    );
  }

  await comment.remove();

  res.status(200).json({
    success: true,
    data: {},
  });
});
